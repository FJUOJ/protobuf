#!/bin/bash

PROTO_DIRS=(
  oj
  service
  judge
  chief
)

for dir in ${PROTO_DIRS[@]}; do
  for p in `find $dir -name "*.proto"`; do
    echo "# $p"
    protoc -I$dir -I$GOPATH/src --go_out=plugins=grpc,paths=source_relative:$dir $p
  done
done
